-- 添加插件格式为：插件[插件作者,插件名]= ｛插件配置｝
return {
    ["goolord/alpha-nvim"] = {
        disable = false,
        config = function()
            require "plugins.configs.alpha"
        end
    },

    ['mvllow/modes.nvim'] = {
        config = function()
            require('modes').setup({
                colors = {
                    copy = "#f5c359",
                    delete = "#c75c6a",
                    insert = "#78ccc5",
                    visual = "#9745be"
                },

                -- Set opacity for cursorline and number background
                line_opacity = 0.15,

                -- Enable cursor highlights
                set_cursor = true,

                -- Enable cursorline initially, and disable cursorline for inactive windows
                -- or ignored filetypes
                set_cursorline = true,

                -- Enable line number highlights to match cursorline
                set_number = true,

                -- Disable modes highlights in specified filetypes
                -- Please PR commonly ignored filetypes
                ignore_filetypes = {'NvimTree', 'TelescopePrompt'}
            })
        end
    },

    -- 平滑滚动
    ["karb94/neoscroll.nvim"] = {
        config = function()
            require("neoscroll").setup()
        end,

        -- lazy loading
        setup = function()
            nvchad.packer_lazy_load "neoscroll.nvim"
        end
    },

    -- 快速分析启动时间
    ["dstein64/vim-startuptime"] = {
        cmd = "StartupTime"
    },

    ['phaazon/hop.nvim'] = {
        branch = 'v1', -- optional but strongly recommended
        config = function()
            -- you can configure Hop the way you like here; see :h hop-config
            require'hop'.setup {
                keys = 'etovxqpdygfblzhckisuran',
                jump_on_sole_occurrence = false
            }
        end
    }

}
