local utils = require "core.utils"

local cmd = vim.cmd
local user_cmd = vim.api.nvim_create_user_command

-- 开启显示相对行
vim.cmd("set relativenumber")

-- 默认光标距离行底默认15行
vim.cmd("set scrolloff=15")

-- 开启行高亮
-- 红色 197
-- 浅蓝色 117
-- 深蓝色 75
-- 绿色 112
-- 紫色 141以及135
-- 字符串颜色是208
-- 光标颜色是191
-- 变量颜色是221
-- 开启水平高亮行
-- vim.cmd("set cursorline")
-- vim.cmd("hi CursorLine guifg=red guibg=blue")
-- :hi CursorLine guifg=red guibg=blue 
-- 开启垂直行高亮
-- vim.cmd("set cursorcolumn")
-- vim.cmd("hi CursorColumn cterm=NONE ctermbg=darkred ctermfg=white guibg=darkred guifg=white")