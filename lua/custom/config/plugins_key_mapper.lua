local map = nvchad.map

-- 插件按键映射

-- 代码跳转插件hop.nvim
-- 搜索2个字母并跳转
map('n', '<leader>f', ":HopPattern<CR>")
-- 显示所有单词前缀并跳转
map('n', 'f', "::HopWord<CR>")
-- 显示所有行单词，并条转到单词前缀
map('n', 'F', ":HopLineStart<CR>")

-- 关闭标签按键映射
map('n', 'zc',":BufferLinePickClose<CR>")
-- 跳转到对应标签
map('n', '<leader>c',":BufferLinePick<CR>")
-- Tap和<C-i>是切换标签
-- 数字跳转
map('n','<leader>1',':BufferLineGoToBuffer 1<CR>')
map('n','<leader>2',':BufferLineGoToBuffer 2<CR>')
map('n','<leader>3',':BufferLineGoToBuffer 3<CR>')
map('n','<leader>4',':BufferLineGoToBuffer 4<CR>')