local map = nvchad.map

-- 设置退出按键Q
map("n", "Q", ":q <CR>")

map("n", "<leader>cc", ":Telescope <CR>")

-- 快速分屏
-- 映射si 为垂直分割
map('n', 'sh', ':set splitright<CR>:vsplit<CR>')
-- 映射sn 为垂直分割
map('n', 'sl', ':set nosplitright<CR>:vsplit<CR>')
-- 映射si 为水平分割
map('n', 'sj', ':set splitright<CR>:split<CR>')
-- 映射si 为水平分割
map('n', 'sk', ':set nosplitright<CR>:split<CR>')
