
-- nvim原生添加按键映射
require("custom.config.native_key_mapper")
-- 插件按键映射
require("custom.config.plugins_key_mapper")
-- 启动nvim命令
require("custom.config.auto_cmd")