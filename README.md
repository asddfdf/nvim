# Neovim配置

> 此配置是基于https://github.com/NvChad/NvChad
>
> neovim中文帮助：https://github.com/glepnir/nvim-lua-guide-zh

## 介绍

- 初次体验neovim时，配置插件管理，安装各种插件对新手及其不友好。

- 国内vim的生态大部分在vim，neovim资源太少。
- 此配置对于自定插件和按键映射十分友好，并且有良好的文件结构。
- 启动速度极快，配置简单。

```
├── LICENSE
├── README.md
├── examples		# 配置例示
│   ├── chadrc.lua
│   └── init.lua
├── init.lua		# neovim 启动入口
└── lua
    ├── core		# 核心文件
    │   ├── autocmds.lua
    │   ├── default_config.lua
    │   ├── mappings.lua
    │   ├── options.lua
    │   └── utils.lua
    ├── custom		# 个人配置目录
    │   ├── chadrc.lua  # 主题, 插件, 插件选项配置
    │   ├── config
    │   │   ├── auto_cmd.lua # 启动配置
    │   │   ├── native_key_mapper.lua # 原生按键命令映射
    │   │   └── plugins_key_mapper.lua # 插件按键映射
    │   ├── init.lua # 个人配置入口
    │   └── plugins
    │       └── init.lua 	# 添加插件配置
    └── plugins		# 默认插件配置目录
        ├── configs
        │   ├── alpha.lua
        │   ├── bufferline.lua
        │   ├── cmp.lua
        │   ├── icons.lua
        │   ├── lsp_installer.lua
        │   ├── lspconfig.lua
        │   ├── lspkind_icons.lua
        │   ├── nvimtree.lua
        │   ├── nvterm.lua
        │   ├── others.lua
        │   ├── statusline.lua
        │   ├── telescope.lua
        │   └── treesitter.lua
        ├── init.lua
        └── packerInit.lua
```



## 快速安装

- 当前插件已在以下OS测试
  - Ubuntu
  - CentOS 8  (Docker环境)
  - Arch

`bash`环境下执行

```bash
git clone https://gitee.com/asddfdf/nvim ~/.config/nvim --depth 1
nvim +'hi NormalFloat guibg=#1e222a' +PackerSync
```

> 安装完成后会自动进入nvim界面并自动拉取插件，安装完成后执行`:Tutor`可获取帮助。

## 卸载

`bash`环境下执行

```bash
rm -rf ~/.config/nvim
rm -rf ~/.local/share/nvim
rm -rf ~/.cache/nvim
```



## 快捷键

在命令界面中输入`:Telescope keymaps`可查看所有快捷键映射

- \<leader\> = 空格

- `n `= 命令模式
- `v` = 选中模式
- `i` = 插入模式
- `C` = `Ctrl`
- `A ` =  `alt`
- `S ` = `shift`

### 原生映射快捷键

- 退出快件键

| 模式 | 快件键 | 说明       |
| ---- | ------ | ---------- |
| n    | Q      | 退出neovim |

- 望远镜(快速预览文件)

| 模式 | 快件键       | 说明     |
| ---- | ------------ | -------- |
| n    | \<leader\>cc | 弹出窗口 |

- 分割窗口


| 模式 | 快件键 | 说明                     |
| ---- | ------ | ------------------------ |
| n    | sl     | 垂直分割窗口，光标不跟随 |
| n    | sh     | 垂直分割窗口             |
| n    | sj     | 水平分割窗口             |
| n    | sk     | 水平分割窗口，光标不跟随 |

### 插件映射快捷键

- 常用快捷键

| 模式 | 快件键          | 说明            |
| ---- | --------------- | --------------- |
| n    | C + s           | 保存文件        |
| n    | C + n           | 开启/关闭文件树 |
| n    | \<leader\> + th | 切换主题        |
| n    | \<leader\> + uu | 更新主题        |

- 搜索快捷键映射

| 模式 | 快件键      | 说明                                             |
| ---- | ----------- | ------------------------------------------------ |
| n    | \<leader\>f | 输入2个字母，并跳转到对应位置                    |
| n    | f           | 显示所有单词标头，输入对应字母，并跳转到对应位置 |
| n    | F           | 显示所有行单词，输入对应到字母，并跳转到对应位置 |

- 标签操作

| 模式 | 快件键     | 说明                                         |
| ---- | ---------- | -------------------------------------------- |
| n    | S + t      | 创建新标签                                   |
| n    | zc         | 标签处显示对应字母，输入字母。关闭对应标签   |
| n    | <leader\>z | 标签处显示对应字母，输入字母。跳转到对应标签 |
| n    | <leader\>1 | 跳转到标签1                                  |
| n    | <leader\>2 | 跳转到标签2                                  |
| n    | <leader\>3 | 跳转到标签3                                  |
| n    | <leader\>4 | 跳转到标签4                                  |

- 唤出终端

| 模式  | 快件键 | 说明             |
| ----- | ------ | ---------------- |
| n/v/i | C + h  | 光标切换到左窗口 |
| n/v/i | C + j  | 光标切换到下窗口 |
| n/v/i | C + k  | 光标切换到上窗口 |
| n/v/i | C + l  | 光标切换到右窗口 |

- 光标跳转

| 模式  | 快件键 | 说明       |
| ----- | ------ | ---------- |
| n/v/i | C + a  | 跳转到行首 |
| n/v/i | C + e  | 跳转到行尾 |

- 唤出终端

| 模式 | 快件键         | 说明             |
| ---- | -------------- | ---------------- |
| n    | A + i          | 唤出浮动终端     |
| n    | A + h          | 显示垂直终端     |
| n    | A + v          | 显示水平终端     |
| n    | \<leader\> + h | 显示新的垂直终端 |
| n    | \<leader\> + v | 显示新的水平终端 |

- 不常用快件键

| 模式 | 快件键          | 说明             |
| ---- | --------------- | ---------------- |
| n    | <leader\>+ n    | 切换行数显示方式 |
| n    | \<leader\> + rn | 关闭行数显示     |

## 插件

- nvim 光标跳转插件
  - 使用`lua`编写
  - https://github.com/phaazon/hop.nvim
  - 使用`Vim Srcipt`编写
  - https://github.com/easymotion/vim-easymotion

nvim开始界面导航

https://github.com/goolord/alpha-nvim

nvim 选中编辑复制时高亮

https://github.com/mvllow/modes.nvim

## 升级

> 警告：如果没有必要最好不要升级，原作者更新十分频繁。每次都需要重新排查插件错误。

- 执行命令

```bash
git clone https://gitee.com/asddfdf/nvim
```

### 高级

如果需要上传自己的配置到git，同时还想更新此插件。可以添加一个新的远程分支，拉取后合并代码

- 执行命令，添加上游分支

```bash
git remote add upstream https://github.com/NvChad/NvChad.git
```

- 更新分支

> 注意：fetch不会自动合并需要手动合并分支

```bash
git fetch upstream && git checkout -b dev-myFEAT upstream/main
# 也可以使用此命令，在本地创建一个tmp临时分支
git fetch upstream main:tmp
```

- 手动合并分支

```bash
# 使用git checkout -b dev-myFEAT upstream/main，使用这个合并
git checkout master && git meger upstream/main
# 使用git fetch upstream main:tmp，使用此命令合并
git checkout master && git meger tmp
```

- 上传到指定远程分支

```bash
git push origin master
```

## 注意事项

- 安装此插件需要安装以下依赖
  - GCC
  - G++
  - Git
  - FZF
- 如果没有正确显示字体，或者一些图标不显示请安装此字体
  - https://www.nerdfonts.com/font-downloads
- 使用此插件最好大于neovim`0.51`否则不能工作
  - 升级地址：https://github.com/neovim/neovim/releases/tag/v0.7.0
- 此nvim插件使用的是`packer.nvim`包管理器。
- 使用此配置最好使用默认主题，自定义主题会遇到不可预知的错误。